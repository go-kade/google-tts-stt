package main

import (
	"context"
	"fmt"
	"log"

	speech "cloud.google.com/go/speech/apiv1p1beta1"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1p1beta1"
)

func main() {
	ctx := context.Background()

	// Instantiates a client
	client, err := speech.NewClient(ctx)

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	defer client.Close()

	// The path to the remote audio file to transcribe
	audioUri := "gs://kadetest/audio-files/1.pcm"

	config := speechpb.RecognitionConfig{
		Model:                 "latest_long",
		Encoding:              speechpb.RecognitionConfig_LINEAR16,
		SampleRateHertz:       48000,
		EnableWordTimeOffsets: true,
		EnableWordConfidence:  true,
		LanguageCode:          "hi-IN",
	}

	audio := speechpb.RecognitionAudio{
		AudioSource: &speechpb.RecognitionAudio_Uri{Uri: audioUri},
	}

	request := speechpb.LongRunningRecognizeRequest{
		Config: &config,
		Audio:  &audio,
	}

	op, err := client.LongRunningRecognize(ctx, &request)
	if err != nil {
		log.Fatalf("failed to recognize: %v", err)
	}
	resp, err := op.Wait(ctx)
	if err != nil {
		log.Fatalf("failed to wait for long-running operation: %v", err)
	}
	// Prints the results
	// fmt.Println("aaaaa",resp.Results)
	for _, result := range resp.Results {
		for _, alt := range result.Alternatives {
			fmt.Printf("%v\n", alt.Transcript)
		}
	}
}
