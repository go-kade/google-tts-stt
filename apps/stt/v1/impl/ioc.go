package impl

import (
	"context"

	speech "cloud.google.com/go/speech/apiv1"
	"gitee.com/go-kade/library/v2/exception"
	"gitee.com/go-kade/library/v2/ioc"
	logs "gitee.com/go-kade/library/v2/ioc/config/log"
	"gitee.com/go-kade/google-tts-stt/apps/stt"
	"github.com/rs/zerolog"
	"google.golang.org/api/option"
)

var _ stt.Service = (*speechToText)(nil)

func init() {
	ioc.Controller().Registry(&speechToText{})
}

type speechToText struct {
	ioc.ObjectImpl
	log    *zerolog.Logger
	client *speech.Client
}

func (t *speechToText) Name() string {
	return stt.AppNameV1
}

func (t *speechToText) Init() error {

	t.log = logs.Sub(stt.AppNameV1)

	t.log.Info().Msg("speech to text v1 client initializing...")
	client, err := speech.NewClient(context.Background(), option.WithCredentialsFile("/Users/kade.chen/Downloads/kade-poc-0c88b6757f62.json"))
	if err != nil {
		t.log.Error().Msgf("speech to text client init failed , error: %v", err)
		return exception.NewUnauthorized("speech to text client init failed, error: %v", err)
	}

	t.client = client

	t.log.Info().Msg("speech to text v1 client successfully initlialized")

	// 初始化 PortAudio
	// err = portaudio.Initialize()
	// if err != nil {
	// 	log.Fatalf("Failed to initialize portaudio: %v", err)
	// }
	return nil
}
