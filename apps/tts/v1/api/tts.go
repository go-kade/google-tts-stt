package api

import (
	"fmt"

	"cloud.google.com/go/texttospeech/apiv1/texttospeechpb"
	"gitee.com/go-kade/google-tts-stt/apps/tts"
	"gitee.com/go-kade/library/v2/exception"
	"gitee.com/go-kade/library/v2/http/response"
	"github.com/emicklei/go-restful/v3"
)

func (h *textToSpeechV1Handler) textToSpeech(r *restful.Request, w *restful.Response) {

	//2.read the request body parametars
	// 从前端读取请求
	h.log.Info().Msg("chenk request parametars...")
	req := tts.NewSynthesizeSpeechRequestBoby()
	if err := r.ReadEntity(&req); err != nil {
		h.log.Error().Msgf("check request parametars failed; error: %v", err)
		response.Failed(w, exception.NewInternalServerError("check request parametars failed; error: %v", err))
		return
	}
	h.log.Info().Msg("chenk request parametars success")
	//3.check the request boby parametars
	if req.Text != "" && req.Ssml != "" {
		h.log.Error().Msgf("Error: Only one can be entered for text and ssml")
		response.Failed(w, exception.NewBadRequest("Error: Only one can be entered for text and ssml"))
		return
	} else {
		switch !req.InputBool {
		case req.Text != "":
			req.SynthesizeSpeechRequest.Input.InputSource = &texttospeechpb.SynthesisInput_Text{
				Text: req.Text}
			h.log.Info().Msgf("SynthesizeSpeechRequest.Input.InputSource.Text: %v", req.Text)
		case req.Ssml != "":
			req.SynthesizeSpeechRequest.Input.InputSource = &texttospeechpb.SynthesisInput_Ssml{
				Ssml: req.Ssml}
			h.log.Info().Msgf("SynthesizeSpeechRequest.Input.InputSource.Ssml: %v", req.Ssml)
		default:
			h.log.Error().Msgf("Error: The value cannot be empty for text and ssml")
			response.Failed(w, exception.NewBadRequest("Error: The value cannot be empty for text and ssml"))
			return
		}
	}

	// 4. call the service
	s, err := h.ttsV1.TextToSpeech(r.Request.Context(), &req.SynthesizeSpeechRequest)
	if err != nil {
		h.log.Error().Msgf("err: %s", &req.SynthesizeSpeechRequest)
		response.Failed(w, exception.NewInternalServerError("create user error: %v", err))
		return
	}
	response.Success(w, fmt.Sprintf("create user success %v", s))
}
