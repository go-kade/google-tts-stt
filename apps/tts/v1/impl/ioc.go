package impl

import (
	"context"

	texttospeech "cloud.google.com/go/texttospeech/apiv1"
	"gitee.com/go-kade/library/v2/exception"
	"gitee.com/go-kade/library/v2/ioc"
	"gitee.com/go-kade/google-tts-stt/apps/tts"
	"github.com/rs/zerolog"
	"google.golang.org/api/option"

	logs "gitee.com/go-kade/library/v2/ioc/config/log"
)

var _ tts.Service = (*textToSpeechV1)(nil)

func init() {
	ioc.Controller().Registry(&textToSpeechV1{})
}

type textToSpeechV1 struct {
	ioc.ObjectImpl
	log    *zerolog.Logger
	client *texttospeech.Client
}

func (t *textToSpeechV1) Name() string {
	return tts.AppNameV1
}

func (t *textToSpeechV1) Init() error {

	t.log = logs.Sub(tts.AppNameV1)

	t.log.Info().Msg("text to speech v1 client initializing...")
	client, err := texttospeech.NewClient(context.Background(), option.WithCredentialsFile("/Users/kade.chen/Downloads/kade-poc-0c88b6757f62.json"))
	if err != nil {
		t.log.Error().Msgf("text to speech client init failed , error: %v", err)
		return exception.NewUnauthorized("text to speech client init failed, error: %v", err)
	}
	t.log.Info().Msg("text to speech v1 client successfully initlialized")

	t.client = client
	return nil
}
