package apps

import (
	//service registry for token
	_ "gitee.com/go-kade/google-tts-stt/apps/stt/v1/impl"
	_ "gitee.com/go-kade/google-tts-stt/apps/tts/v1/impl"

	//api
	_ "gitee.com/go-kade/google-tts-stt/apps/stt/v1/api"
	_ "gitee.com/go-kade/google-tts-stt/apps/tts/v1/api"
	//注册所有provider
)
